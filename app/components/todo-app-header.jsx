import React from 'react';

export default class TodoAppHeader extends React.Component {
  constructor(props) {
    super(props);

    this.handleToggleAllTask = this.handleToggleAllTask.bind(this);
    this.handleNewTaskTextChange = this.handleNewTaskTextChange.bind(this);
  }

  handleToggleAllTask(e) {
    this.props.onHandleToggleAllTask(e.target.checked);
  }

  handleNewTaskTextChange(e) {
    this.props.onHandleNewTaskTextChange(e.target.value);
  }

  render() {
    const doneTack = this.props.tasks.filter((task) => task.done === true);
    const doneAll = doneTack.length === this.props.tasks.length;

    return (
      <header className="header">
        <h1 className="header__title">Список задач</h1>
        {this.props.tasks.length !== 0 ?
          <input type="checkbox"
                 className="checkbox header__toggle-all"
                 checked={doneAll}
                 onChange={this.handleToggleAllTask}/> : ''}
        <input type="text"
               className="text header__new-todo"
               placeholder="Что нужно сделать?"
               id="text"
               autoComplete="off"
               autoFocus
               value={this.props.taskText}
               onKeyUp={this.props.onHandleNewTaskTextSubmit}
               onChange={this.handleNewTaskTextChange}/>
        <label htmlFor="text"
               className="header__enter">&crarr;</label>
      </header>
    );
  }
}
